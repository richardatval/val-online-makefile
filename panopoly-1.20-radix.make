core = 7.x

api = 2
projects[drupal][version] = "7.34"

; Modules Contrib

projects[addressfield] = "1.0"
projects[addressfield_staticmap] = "1.0"
projects[better_exposed_filters
projects[boost] = "1.0"] = 
projects[bootstrap_library][version] = "1.4"
projects[conditional_styles][version] = "2.2"
projects[cookiecontrol] = "1.6"
projects[custom_formatters][version] = "2.2"
projects[ds][version] = "2.x-dev"
projects[email][version] = "1.3"
projects[feeds][version] = "2.0-alpha8"
projects[feeds_tamper][version] = "1.0"
projects[field_collection][version] = 
projects[field_collection_fieldset[version] = 
projects[field_group][version] = 
projects[fontyourface] = "2.8"
projects[ftools] =
projects[globalredirect] = "1.5"
projects[google_analytics] = "2.1"
projects[honeypot] = "1.17"
projects[httprl] = "1.14"
projects[job_scheduler][version] = "2.0-alpha3"
projects[linkchecker] = "1.2"
projects[mailchimp] = "3.2"
projects[mailchimp][version] = "3.2"
projects[menu_position] = "1.1"
projects[metatag] = "1.4"
projects[node_expire] = "1.7"
projects[node_expire][version] = "1.7"
projects[node_export][version] = "3.0"
projects[panopoly_demo][version] = "1.14"
projects[pathologic] = "2.12"
projects[prepopulate] = "2.0"
projects[radix_admin] = "3.x-dev"
projects[radix_layouts] = "3.3"
projects[redirect] = "1.0-rc1"
projects[rules] = "2.7"
projects[scanner] = "1.x-dev"
projects[search404] = "1.3"
projects[site_verify] = "1.1"
projects[views_bootstrap][version] = "3.1"
projects[views_dependent_filters][version] = "1.x-dev"
projects[views_infinite_scroll] = "1.1"
projects[views_rss][version] = "2.0-rc3"
projects[webform] = "4.7"
projects[webform][version] = "3.21"
projects[xmlsitemap] = "2.1"

; Modules Custom

projects[lol_core_feature][download][type] = "git"
projects[lol_core_feature][download][url] = "git@bitbucket.org:richardatval/lol_core_feature.git"
projects[lol_core_feature][type] = "module"
projects[lol_core_feature][download][branch] = "master"

projects[lol_directory_feature][download][type] = "git"
projects[lol_directory_feature][download][url] = "git@bitbucket.org:richardatval/lol_directory_feature.git"
projects[lol_directory_feature][type] = "module"
projects[lol_directory_feature][download][branch] = "master"

projects[lol_news_and_events_feature][download][type] = "git"
projects[lol_news_and_events_feature][download][url] = "git@bitbucket.org:richardatval/lol_news_and_events_feature.git"
projects[lol_news_and_events_feature][type] = "module"
projects[lol_news_and_events_feature][download][branch] = "master"

projects[lol_info_advice_feature][download][type] = "git"
projects[lol_info_advice_feature][download][url] = "git@bitbucket.org:richardatval/lol_info_advice_feature.git"
projects[lol_info_advice_feature][type] = "module"
projects[lol_info_advice_feature][download][branch] = "master"

projects[val_online_content_types_categories][download][type] = "git"
projects[val_online_content_types_categories][download][url] = "git@bitbucket.org:richardatval/val_online_content_types_categories"
projects[val_online_content_types_categories][type] = "module"
projects[val_online_content_types_categories][version] = "1.0-beta1"

projects[val_e_briefing][download][type] = "git"
projects[val_e_briefing][download][url] = "git@bitbucket.org:richardatval/val_e_briefing.git"
projects[val_e_briefing][type] = "module"
projects[val_e_briefing][version] = "1.0-beta1"

projects[val_online_fieldable_panel_panes_config][download][type] = "git"
projects[val_online_fieldable_panel_panes_config][download][url] = "git@bitbucket.org:richardatval/val_online_fieldable_panel_panes_config"
projects[val_online_fieldable_panel_panes_config][type] = "module"
projects[val_online_fieldable_panel_panes_config][version] = "1.0-beta1"

projects[val_online_slideshow][download][type] = "git"
projects[val_online_slideshow][download][url] = "git@bitbucket.org:richardatval/val_online_slideshow"
projects[val_online_slideshow][type] = "module"
projects[val_online_slideshow][download][branch] = "master"
projects[val_online_slideshow][version] = "1.0-beta1"

projects[valonline_fpp][download][type] = "git"
projects[valonline_fpp][download][url] = "git@bitbucket.org:richardatval/valonline_fpp"
projects[valonline_fpp][type] = "module"
projects[valonline_fpp][download][branch] = "master"
projects[valonline_fpp][version] = "1.0-beta1"

projects[book_index][download][type] = "git"
projects[book_index][download][url] = "git@bitbucket.org:richardatval/book_index"
projects[book_index][download][branch] = "master"
projects[book_index][type] = "module"

projects[book_index][download][type] = "git"
projects[book_index][download][url] = "git@bitbucket.org:richardatval/book_index"
projects[book_index][download][branch] = "master"
projects[book_index][type] = "module"

projects[things_to_do][version] = "1.x-dev"
projects[things_to_do][download][type] = "git"
projects[things_to_do][download][url] = "git@bitbucket.org:richardatval/things_to_do"
projects[things_to_do][type] = "module"

projects[lol_features][download][type] = "git"
projects[lol_features][download][url] = "git@bitbucket.org:richardatval/lol_features.git"
projects[lol_features][type] = "module"
projects[lol_features][version] = "1.0-beta1"

; Themes

projects[radix][version] = "3.0-beta2"

projects[choiceadvice][download][type] = "git"
projects[choiceadvice][download][url] = "git@bitbucket.org:richardatval/choiceadvice"
projects[choiceadvice][download][branch] = "master"
projects[choiceadvice][type] = "theme"

projects[localofferleicester][download][type] = "git"
projects[localofferleicester][download][url] = "git@bitbucket.org:richardatval/localofferleicesternew.git"
projects[localofferleicester][download][branch] = "master"
projects[localofferleicester][type] = "theme"

projects[sendiass][download][type] = "git"
projects[sendiass][download][url] = "git@bitbucket.org:richardatval/SENDIASS"
projects[sendiass][download][branch] = "master"
projects[sendiass][type] = "theme"

projects[valonline][download][type] = "git"
projects[valonline][download][url] = "git@bitbucket.org:richardatval/valonline"
projects[valonline][download][branch] = "master"
projects[valonline][type] = "theme"

projects[values][download][type] = "git"
projects[values][download][url] = "git@bitbucket.org:richardatval/values"
projects[values][download][branch] = "master"
projects[values][type] = "theme"

; Libraries

libraries[autopager][download][type] = "get"
libraries[autopager][download][url] = "http://jquery-autopager.googlecode.com/files/jquery.autopager-1.0.0.js"
libraries[autopager][directory_name] = "autopager"
libraries[autopager][type] = "library"

libraries[bootstrap][download][type] = "git"
libraries[bootstrap][download][url] = "git@bitbucket.org:richardatval/bootstrap-3.03-drupal"
libraries[bootstrap][directory_name] = "bootstrap"
libraries[bootstrap][type] = "library"

libraries[mailchimp][download][type] = "get"
libraries[mailchimp][download][url] = "https://bitbucket.org/mailchimp/mailchimp-api-php/get/7ac99b5ac746.zip"
libraries[mailchimp][directory_name] = "mailchimp"
libraries[mailchimp][type] = "library"

libraries[respondjs][download][type] = "get"
libraries[respondjs][download][url] = "https://github.com/scottjehl/Respond/blob/master/dest/respond.min.js"
libraries[respondjs][directory_name] = "respondjs"
libraries[respondjs][type] = "library"